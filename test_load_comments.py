#
# load comments test
# from csv file
# 

import csv
from static.config import settings
import os

def load_comments():
    comments = []
    path = os.path.abspath(os.path.join(settings["data_path"], "comments.csv"))
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            comments.append(row)
    return comments

if __name__ == "__main__":
    comments = load_comments()
    for com in comments:
        print(com["postid"] + " : " + com["text"]  )
