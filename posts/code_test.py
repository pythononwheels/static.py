

post = {

	"publish" 		: "2016-02-12T12:00:00",
	"created" 		: "2016-02-12T12:00:00",
	"comments" 		: "off",
	"category"		: "news",
	"author"		: "klaas",
	"tags"			: ["python", "devel", "code"],
	"story_img"     : "highlightjs.png",
	"title"			: "Inline code test",
	"text"			: """

#### a code example:

	@requires_authorization
	def somefunc(param1='', param2=0):
	    r'''A docstring'''
	    if param1 > param2: # interesting
	        print ("Greater")
	    return (param2 - param1 + 1 + 0b10l) or None

	class SomeClass:
	    pass

	>>> message = '''interpreter
	... prompt'''

Just to see if inline code in posts works well.
I like it.


Uses [highlight.js](https://highlightjs.org/)

Of course you can use any [style you like](https://highlightjs.org/static/demo/) ..

Download the [css files from github](https://github.com/isagalaev/highlight.js/tree/master/src/styles)

"""

	
}
