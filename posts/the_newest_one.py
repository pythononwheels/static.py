

post = {

    "publish"       : "2016-01-31T23:30:00",
    "created"       : "2016-01-31T12:00:00",
    "comments"      : "off",
    "category"      : "news",
    "author"        : "klaas",
    "tags"          : ["python", "static", "first", "wow"],
    "title"         : "This should come first !! (till 31.1.2016)",
    "subtitle"      : "wonferful feeling.",
    "story_img"     : "Titan_poster.png",
    "featured"      : True,
    "text"          : """

Well, I always wanted to do this and now I did.

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy 
eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, 
no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore 
magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 

Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

### Just Titanic:

look at this beauty:

![Alt text](/static/images/Titan_poster_840.png)


**Titan** (auch *Saturn VI*) ist mit einem Durchmesser von 5150&nbsp;Kilometern der größte [Mond] des Planeten [Saturn], weshalb er nach dem Göttergeschlecht der [Titanen] benannt wurde. Er ist ein [Eismond], nach [Ganymed] der [zweitgrößte Mond] im [Sonnensystem] und der einzige mit einer dichten [Gashülle].

Titan wurde 1655 vom niederländischen Astronomen [Christiaan Huygens] entdeckt.[1] Beobachtungen von der [Erde] und vom [Weltraumteleskop Hubble] aus erweiterten das Wissen über ihn, insbesondere jedoch Vorbeiflüge einiger [Raumsonden] seit 1979. Die informativsten Bilder und Messdaten sind bei der Landung der Sonde [Huygens] im Jahre 2005 erfasst worden.[2]

Obwohl die Oberflächentemperatur des Titans weitaus niedriger ist als die der Er

[1] 

[2] 

  [Mond]: Satellit_(Astronomie) "wikilink"
  [Saturn]: Saturn_(Planet) "wikilink"
  [Titanen]: Titan_(Mythologie) "wikilink"
  [Eismond]: Eismond "wikilink"
  [Ganymed]: Ganymed_(Mond) "wikilink"
  [zweitgrößte Mond]: Liste_der_Monde_von_Planeten_und_Zwergplaneten "wikilink"
  [Sonnensystem]: Sonnensystem "wikilink"
  [Gashülle]: Atmosphäre_(Astronomie) "wikilink"
  [Christiaan Huygens]: Christiaan_Huygens "wikilink"
  [Erde]: Erde "wikilink"
  [Weltraumteleskop Hubble]: Hubble-Weltraumteleskop "wikilink"
  [Raumsonden]: Raumsonde "wikilink"
  [Huygens]: Cassini-Huygens "wikilink"

"""

    
}
