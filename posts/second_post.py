

post = {

	"publish" 		: "2016-01-24T23:30:00",
	"created" 		: "2016-01-22T12:00:00",
	"comments" 		: "off",
	"category"		: "news",
	"author"		: "klaas",
	"tags"			: ["python", "static"],
	"title"			: "A second post with static.py",
	"subtitle"		: "wonferful feeling.",
	"story_img"		: "640px-Dioctria_linearis_-_side_(aka).jpg",
	"featured"		: True,
	"text"			: """

## headline2	

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy 
eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, 
no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore 
magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 

Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

### wow, this is an image:

![Alt text](/static/images/LimacinaHelicinaNOAA.jpg)

"""

	
}
