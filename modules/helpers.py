#
#
# place all general helper functions here
#
#
import markdown2 as markdown

def page_text(page=None):
    print("   ... helper: page_text called")
    return page["markdown_text"].strip()


CONST_TEXT = "A CONSTANT TEXT"