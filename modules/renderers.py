from static.config import settings
from PIL import Image
import os

class GalleryPreRenderer():

    '''
        If a rederer is defined in pages or posts.
        (See gallyery page in the example blog.)
         
            "pre_renderer"  : GalleryPreRenderer,

        Its run method will be called before the page or post is
        finally rendered.

        The returned value will be available in the page's or post's    
        template via the variable name <custom>

        so the image_list below is available in the template as:

            {% for image in custom %}
                ...
                <img width="{{image["width"]}}" height="{{image["height"]}}" src="/static/images/{{image["name"]}}" alt="{{image["alt"]}}"> 
                <p>{{image["alt"]}}</p>
              ... 
            {% end %}
    '''
    def run(self, post=None, page=None):
        """
            process all images in static/images 
            generate a dict with
            image {
                name
                width
                height
            }
        """
        exclude_files = ["__init__.py"]
        images = [f for f in os.listdir(settings["image_path"]) 
            if (os.path.isfile(os.path.join(settings["image_path"], f)) and f not in exclude_files)]
        image_list = []
        #print(" ... {0:5s} x {1:5s} {2:30s}".format("width", "height", "name"))
        #print(50*"-")
        for image in images:
            imagepath = os.path.join(settings["image_path"],image)
            try:
                im = Image.open(imagepath)
                width, height = im.size
                im = { "name" : image, "path" : imagepath, "width" : width, "height" : height, "alt" : image}
                image_list.append(im)    
            except Exception as e:
                print(" ... Error: GalleryPreRenderer: " + str(e))
            #print(" ... {0:5d} x {1:5d} {2:30s}".format(im["width"], im["height"], im["name"]))
        
        return image_list
