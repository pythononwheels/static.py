### See PIN-based authorization for details at
### https://dev.twitter.com/docs/auth/pin-based-authorization
## taken from here: https://gist.github.com/hezhao/4772180

import tweepy
import pprint
import hashlib
from static.config import settings
# what about sharing these keys ?? for a cli app you cant do anything else for now.
# not too risky but also not welldesigned by twitter. 
# more info. read this: http://arstechnica.com/security/2010/09/twitter-a-case-study-on-how-to-do-oauth-wrong/
consumer_key="Qd5Ep7yiQ72NLRi5RdGi1pKEY"
consumer_secret="JSG7vGdIbfsEPEaZRWLDPMpGduhiNpCeLruFYI1ZkzAGCrxr7u"

pp = pprint.PrettyPrinter(indent=4)

def get_parameter(text, tag):
    '''
        given a text returns the string following the 
        give tag or None if tag is not found.

        Example: text => text.strip().split() => ["a", "#b" , "c", "d" ]
        get_paramter("a #b c d", "#b") => returns c
    '''
    textlist = text.strip().split()
    try:
        return textlist[textlist.index(tag)+1]
    except (ValueError, IndexError) as e:
        # tag not found or no parameter after tag
        return None
    except:
        # something else (most p)
        return None

def clear_comment(text, postid):
    """
        get rid of the #comment, #postid and hexdigest
    """
    tl = text.lower().split()
    try:
        tl.remove("#comment")
        tl.remove("#postid")
        tl.remove("@"+settings["twitter"])
        tl.remove(str(postid))
    except ValueError:
        print(" ... Error: which shouldnt happen. Comments should always have all values here")
    return " ".join(tl)

def get_comments():
    """
        get all(possible)  comments.
        Will read the last 100 mentions (tweets with @username)
        Checks if those have the #comment AND #postid tag
        => these will be considered.

        The key to link a comment to a post is the md5 hexdigest
        followinfg the #postid tag. 
    """
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)

    # get access token from the user and redirect to auth URL
    auth_url = auth.get_authorization_url()
    print(50*"-")
    print("... Getting the comments: ...")
    print(50*"-")

    print(50*"-")
    print(" Please copy & paste this URL to a browser and authorize static.py to get the comments")
    print()
    print(" " + auth_url)
    print()
    #print(" ... ... static.py will only parse your last 100 mentions to get the blog comments")
    

    # ask user to verify the PIN generated in broswer
    verifier = input(' Enter the PIN and press <enter>: ').strip()
    auth.get_access_token(verifier)
    #print(dir(auth.access_token))
    #print ("ACCESS_KEY = {0:50s}".format(str(auth.access_token)))
    #print ("ACCESS_SECRET = {0:50s}".format(str(auth.access_token_secret)))

    # authenticate and retrieve user name
    auth.set_access_token(auth.access_token, auth.access_token_secret)
    api = tweepy.API(auth)
    username = api.me().name
    print (' Collecting comments adressed to:' + username)
    print(50*"-")
    print("mentions:")
    print(50*"-")

    mentions = api.mentions_timeline(count=100)
    comment_list = []

    for mention in mentions:
        #print(" ... " + str(type(mention.text)) + str(mention.text))
        txt = mention.text.lower()
        comment = get_parameter(txt, "#comment") 
        postid = get_parameter(txt, "#postid")

        if comment and postid:
            #print("FOUND comment for postid: " + str(postid))
            #print(str(mention.text).encode("utf-8"))
            cleared = clear_comment(mention.text, postid)
            pkey=comment+postid+mention.user.screen_name+str(mention.created_at)
            hashval = hashlib.md5(pkey.encode("utf-8")).hexdigest()
            comment_list.append( {   
                    "user"          : mention.user.screen_name,
                    "raw_text"      : mention.text,
                    "postid"        : postid,
                    "text"          : cleared,
                    "profile_img_url"   : mention.user.profile_image_url_https,
                    "created_at"       : mention.created_at,
                    "hash"          : hashval
                    #"raw_mention"   : mention
                })
                
            #print(mention.user.screen_name)
    return comment_list
    #print(50*"-")
    #print("rate_limit:")
    #print(50*"-")
    #pp.pprint(str(api.rate_limit_status()))