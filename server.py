#!/usr/bin/python3
#
# static.py simple testserver.
# doesnt need more since we are static ;)
# khz 2016
#
import os
from static.config import settings
from http.server import HTTPServer, SimpleHTTPRequestHandler

os.chdir(os.path.normpath(settings["site_path"]))

port = 8000
host_name = "localhost"
httpd = HTTPServer((host_name, port), SimpleHTTPRequestHandler)
print("static.py (test)server started, to quit press <ctrl-c>")
print(" ... serving from: http://" + host_name + ":" + str(port))
print(os.getcwd())
httpd.serve_forever()
