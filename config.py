#
# static.py settings
#

settings = {
    #
    # test http server 
    #
    "port"              :   8000,
    "debug"             :   True,
    
    #
    # paths
    #
    "template_path"     :   "layouts",
    "static_path"       :   "static",
    "image_path"        :   "static/images",
    "static_url_prefix" :   "static",
    "post_path"         :   "posts",
    "page_path"         :   "pages",
    "tagview_path"      :   "tagviews",
    "data_path"         :   "data",
    "site_path"         :   "site",
    
    #
    # general
    # 
    "autoescape"        :   False,
    "use_absolute_urls" :   False,      # not implemented yet. (Feb, 2016)
    "permalink"         :   "title",    # title or filename(not implemented yet, Feb 2016)
    "index_filename"    :   "index",
    "hashtrunc"         :   13,         # length to truncate the md5 postid hash 

    #
    # blog
    #
    "base_url"          :   "http://pythononwheels.github.io",
    "name"              :   "pythononwheels",           # shown top-left (like home)
    "page_title"        :   "My wonderful static blog", # html title
    "blog_title"        :   "static.py", # blog title
    "show_title"        :   True,
    "show_subtitle"     :   True,
    "blog_subtitle"     :   "the easy and quick blog, page and single page generator. This smaple blog is made with static.py",
    #"categories"        :   [   ("About/Projects","/pages/what-this-is-all-about.html"), 
    #                            ("static.py","/pages/static_page.html"), 
    #                            ("Gallery","/pages/image-gallery.html"),
    #                            ("Charttest","/pages/charttest.html")
    #                        ],
    "enable_twitter_comments"   :   True,

    #
    # templates
    #
    "blog_tmpl"         :   "bs4_blog/blog_post_summary.tmpl",  # used to render the blog overview
    "post_tmpl"         :   "bs4_blog/blog_post.tmpl",          # used for a single post page
    "tags_view_tmpl"    :   "bs4_blog/blog_post_summary.tmpl",  # used for the tags_view (click on a tag)
    
    #
    # post 
    #
    "encoding"                  :   "utf-8",
    "author"                    :   "klaas",
    "summary_text_length"       :   400,                # number of character shown as blog post summary 
    "summary_image_width"       :   200,                # used in the post summary list (style is always thumbnails)
    "post_image_style"          :   "img-thumnail",     # default style used in the post  if none is given in the post
    "about"                     :   "this is a sample blog for static.py. Everything around python, computers, science, news, and stuff.",
    "posts_per_page"            :   3,                  
    "show_featured"             :   True,              # it True shows featured posts in the blog summary
    "featured_interval"         :   6000,               # milliseconds
    "featured_autoplay"         :   "on",               # or off
    "show_story_img"            :   False,
    
    #
    # pages
    #

    #
    # social
    #
    "twitter"           :   "pythononwheels",
    "github"            :   "pythononwheels",
    "googleplus"        :   None,
    "facebook"          :   None
}

