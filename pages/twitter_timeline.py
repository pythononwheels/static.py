

page = {

    "publish"       : "2016-01-22T12:00:00",
    "created"       : "2016-01-22T12:00:00",
    "author"        : "klaas",
    "template"      : "bs4_blog/twitter.tmpl",
    "title"         : "@pythononwheels twitter timeline",
    "nav_link"      : True,
    "nav_link_name" : "Twitter",
    "permalink"     : "twitter-timeline",
    "text"          : [
"""
    <style>
        .wb-twitter iframe[style] {
            width: 100%;
        }
    </style>
      <a class="twitter-timeline" width="520" href="https://twitter.com/pythononwheels" data-widget-id="245262220982882304">Tweets von @pythononwheels</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

"""]
}
