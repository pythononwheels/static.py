
page = {

    "publish"       : "2016-01-15T12:00:00",
    "created"       : "2016-02-14T12:00:00",
    "author"        : "klaas",
    "template"      : "bs4_blog/standard_page.tmpl",
    "title"         : "static.py",
    "story_img"     : "static_logo_2.png",
    "image_width"   : "200px",
    "subtitle"      : "THE EASY AND QUICK BLOG, PAGE AND SINGLE PAGE GENERATOR.",
    "nav_link"      : True,
    "nav_link_name" : "static",
    "permalink"     : "static_page",
    "text"          : [
"""
![static.py logo](/static/images/static_logo_2.png)

### static.py 
is (yet another) static blog/page/singlepage generator for python.

But I think it has quite a few nice features to make you up and running out of the box.
The most handy is IMHO that you can comment static post with twitter. The next time you generate
the blog the comments are collected and rendered automatically. 
You can also update and render the blog in the backend every some minutes to be more up-to-date.

**if you want to make a blog or single page => you're done! (batteries included ;)**

#### unique features

* **comment posts via twitter** 
..* so no third party tool or login required for comments.
..* comments are collected automatically from twitter next time you generate the blog.

* pre rendered **tag views** on board (click on a tag and you see all posts with that tag)

#### features
* supports **blogs, pages, posts and single pages**
* **tagging** on board 
* [tag cloud](https://github.com/addywaddy/jquery.tagcloud.js/) on board (see snapshots below)
* **simple pagination** (newer, older) on board
* **syntax highlighting** with [highlight.js](https://highlightjs.org/) on board
* [charts.js](http://www.chartjs.org/) on board and usable in the posts / pages directly
* [d3.js](https://d3js.org/) demo with loading data from csv file on board. 
* everthing in posts and pages is [markdown](https://daringfireball.net/projects/markdown/)
* you can add your own javascript or css in every post or template
* **pre_rederers**: call any of your own python functions before rendering
* based on **[bootstrap4](http://v4-alpha.getbootstrap.com/)**
* very sophisticated **template engine** based on [tornado](http://www.tornadoweb.org/) {{val}}
* [image gallery](http://sapegin.github.io/jquery.mosaicflow/) on board
* ready to be hosted on **github pages** (as this site is)

#### the goal is to make you just run in a second if you wish to make

* a blog
* a single page or landing page
* specific pages for a topic including charts, articles.

#### workflow
0. (generate your site) 
    
         python generate_site.py (or just copy the dirs for now)


1. write a post or page (markdown text).
    The example below is this exactr page you can see right now
    
    ![page example](/static/images/sample_page.png)

2. generate the blog


        python generate_blog.py



3. run test_server (if happy copy to your server, webhoster or [github](https://pages.github.com/)
4. done

#### looks like this with the default theme (bs4_blog)
![static_blog_post_example](/static/images/static_blog_post_example.png)


"""
]
}
