page = {

    "publish"       : "2016-01-22T12:00:00",
    "created"       : "2016-01-22T12:00:00",
    "author"        : "klaas",
    "template"      : "bs4_blog/standard_page.tmpl",
    "nav_link"      : True,
    "nav_link_name" : "Charttest",
    "title"         : "Charttest",
    "subtitle"      : "is just me, my interests and my projects",
    "intro"         : None,
    "text"          : [
"""
# A [d3.js](https://d3js.org/) chart loading csv from data/

Showing the tweetchazzer Games and Moves over the last 30 days.
(Updated dayly)

The "span" is defined in the page/charttest.py and the d3.js javascript
chart is entirely defined in the template (charttest.tmpl)
The data is loaded on the fly (even in this static page) from 

    data/game_stats.csv


    d3.csv("/data/game_stats.csv", function(error, data) {
        ...
    }

<span class="chart-span">
</span>

#### And here an example in [chart.js](http://www.chartjs.org/)

The data and javascript for this chart are entirely defined in the
page markdown. (see pages/charttest).

<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
<canvas id="buyers" width="600" height="400"></canvas>
<script>
var buyerData = {
    labels : ["January","February","March","April","May","June"],
    datasets : [
        {
            fillColor : "rgba(172,194,132,0.4)",
            strokeColor : "#ACC26D",
            pointColor : "#fff",
            pointStrokeColor : "#9DB86D",
            data : [203,156,99,251,305,247]
        }
    ]
}
    var buyers = document.getElementById('buyers').getContext('2d');
    new Chart(buyers).Line(buyerData);
</script>



### finally a jquery table loading a csv file.


"""]
}
