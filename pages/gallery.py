from modules.renderers import GalleryPreRenderer

page = {

    "publish"       : "2016-02-05T11:00:00",
    "created"       : "2016-02-05T10:00:00",
    "author"        : "klaas",
    "template"      : "bs4_blog/gallery.tmpl",
    "nav_link"      : True,
    "nav_link_name" : "Gallery",
    "title"         : "image gallery",
    "subtitle"      : "",
    "story_img"     : "",
    "intro"         : None,
    "pre_renderer"  : GalleryPreRenderer,
    "text"          : [""]
}