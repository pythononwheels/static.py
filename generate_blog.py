#
# generate_blog
#
import datetime
from dateutil.relativedelta import *
from dateutil.parser import parse

import os, imp
from os.path import isfile, join
import json
import pprint
import argparse
import markdown2 as markdown
#import markdown as markdown
import hashlib
import csv
import json
from operator import itemgetter

from tornado import template

from static.config import settings
import static.modules.twitter_comments as twitter_comments

pp = pprint.PrettyPrinter(indent=4)


# configure the template loader (http://www.tornadoweb.org/en/stable/template.html#tornado.template.Loader)
if settings["autoescape"]:
    loader = template.Loader("./layouts")
else:
    loader = template.Loader("./layouts", autoescape=None)
#
# constructing the site_path
#
site_path = None
if settings["use_absolute_urls"]:
    site_path = settings["base_url"] + settings["site_path"]
else:
    pass


def publish_post(post):
    '''
        check if publish date is older than today/now
        if so = publish post
    '''
    dt1 = parse(post["publish"])
    dt2 = datetime.datetime.now()
    if dt2>dt1:
        return True
    else:
        return False

def get_delta_days(post):
    '''
        calculate the age of a post in days
    '''
    dt = parse(post["publish"])
    delta = relativedelta(datetime.datetime.now(), dt)
    days = (delta.years * 365 ) + delta.days
    return days

def render_tag_views(tagsdir, categories=None):
    '''
        renders the tag views containing all posts with a specific tag.
        The default template is the same as the blog list view
    '''
    print(50*"-")
    print("... Rendering the tag views: ")
    print(50*"-")
    for tag in tagsdir:
        #
        # now render the post
        #
        # take post layout from config settings.post_tmpl
        loader = template.Loader("./layouts")
        post_list = sorted(tagsdir[tag], key=itemgetter('created'), reverse=True) 
        out = loader.load(settings["tags_view_tmpl"]).generate(postlist=post_list, show_subtitle=False, tags=tagsdir,
            show_featured=False, show_header=False, categories=categories)
        opath = os.path.join(os.path.join(settings["site_path"]+"/tagviews/"), str(tag)+".html")
        ofile = open(opath,"wb")
        print(" ... rendered: " + opath)
        ofile.write(out)
        ofile.close()

def render_blog_summary_with_pagination(post_list, featured, tagsdir, categories=None):
    '''
        the main view!
        as the name says. render the blog post "list" with the
        simple newer / older pagination.

    '''
    # sort the post by date created
    post_list = sorted(post_list, key=itemgetter('created'), reverse=True) 
    
    #loader = template.Loader("./layouts")
    posts_per_page = settings["posts_per_page"]
    
    fname = settings["index_filename"]
    num_runs = round(len(post_list)/posts_per_page)
    carry = len(post_list) % posts_per_page  #why carry (tribute) see: https://en.wikipedia.org/wiki/Carry_flag

    print(50*"-")
    print("... Rendering the post list: ")
    print(50*"-")
    print(" ... num runs: " + str(num_runs) + " ... offset: " + str(carry))
    for run in range(0,num_runs):
        start_idx = run*posts_per_page
        end_idx = start_idx + posts_per_page
        #print("     start_idx: {0:3d} end_idx: {1:3d}".format(start_idx, end_idx))
        if run == 0:
            page_before = None
        elif run == 1:
            page_before = fname + ".html"
        else:
            # run > 1
            page_before = fname + "_" + str(run-1) + ".html"

        if num_runs-1 > run:
           page_after = fname + "_" + str(run+1) + ".html"
        else:
            page_after = None
        print("     page_before: {0:10s} page_after: {1:10s}".format(str(page_before), str(page_after)))
        
        out = loader.load(settings["blog_tmpl"]).generate(postlist=post_list[start_idx:end_idx], tags=tagsdir,
            featured=featured, show_subtitle=True, show_featured=settings["show_featured"], show_header=True,
            page_before=page_before, page_after=page_after, site_path=site_path, categories=categories)
        
        if run > 0:
            ofilename  = fname + "_" +str(run)
        else:
            ofilename = fname
        ofile = open( os.path.join(settings["site_path"],ofilename + ".html"),"wb")
        print(" ... run: " +str(run) + " ... writing : " + ofilename)
        ofile.write(out)
        ofile.close()

def render_posts(exclude_files, comments=None, categories=None):
    '''
        render all posts
        create featured list
        order posts by date (created)
        create a special page for each tag where all posts are listed that have this tag

        A post:
        * is listed in the blog summary
        * has a permalink url (title with dashed instead of spaces)
        * has tags and is listed on that tags page
        * moves (down) when new(er) post are published
        * can be featured and will then be shown in the featured widget.
        * ONE post in the blog can be made sticky and will stay on top. (use this wisely ;)

    '''
    print(50*"-")
    print("... Rendering the posts: ...")
    print(50*"-")

    posts = [f for f in os.listdir(settings["post_path"]) if (isfile(join(settings["post_path"], f)) and f not in exclude_files)]
    featured = []
    post_list = []
    tagsdir = {}
    #
    # prepare the tags dict
    #
    for f in posts:
        filepath = os.path.join(settings["post_path"],f)
        mod_name,file_ext = os.path.splitext(os.path.split(filepath)[-1])
        #print("mod_name: " + str(mod_name) + " ::: " + str(file_ext) )
        py_mod = None
        if file_ext.lower() == '.py':
            py_mod = imp.load_source(mod_name, filepath)
        
        if hasattr(py_mod, "post"):
            post = getattr(py_mod, "post")
            # check every post for publish date vs today.
            # if ok, add post to post_render_list
            print("... render post: {0:30s}".format(post["title"][0:30]), end="")
            if publish_post(post):
                #pp.pprint(post)
                post["delta_days"] = str(get_delta_days(post))
                post["markdown_text"] = markdown.markdown(post["text"].strip())
                if "intro" in post:
                    post["intro"] = markdown.markdown(post["intro"].strip())
                #post["permalink"] = hashlib.md5(post["title"].encode(settings["encoding"])).hexdigest() + ".html"
                post["permalink"] = post["title"].lower().strip().replace(".", "").replace(" ", "-") + ".html" 
                if not "featured" in post:
                    post["featured"] = False
                
                #
                # add to tags list
                #
                for tag in post["tags"]:
                    if tag in tagsdir:
                        # already there so add the post
                        tagsdir[tag].append(post)
                    else:
                        # first post with that tag, so create the list
                        tagsdir[tag] = [post]

                #print("...delta_days::: " + post["delta_days"])
                post_list.append(post)
                print(" ... published ...", end="")
                if post["featured"]:
                    featured.append(post)
                    print(" ... featured ...")
                else:
                    print()
            else:
                print(" ... not published ...")
        else:
            print(" ... has no attribute post ... " + mod_name)
    print()
    #for elem in featured:
    #    print("   ... featured: " + elem["intro"])
        # take blog layout from config settings.blog_tmpl
    
    #
    # now render the posts
    #
    # take post layout from config settings["post_tmpl"]
    print(50*"-")
    print(" ... Rendering Comments:")
    print(50*"-")
    for post in post_list:
        import hashlib
        postid=hashlib.md5(str(post["title"]).encode("utf-8")).hexdigest()[:settings["hashtrunc"]]
        print(" ... hash: " + postid)
        post_comments_list = None
        if settings["enable_twitter_comments"]:
            # check if this one has a comment
            post_comments_list = [c for c in comments if c["postid"] == postid]
        if post_comments_list:
            print(" ... --> found " + str(len(post_comments_list))  + " comments for this post")
        post["comments"] = post_comments_list
        #print(" ... categories:{}".format(str(categories)))
        out = loader.load(settings["post_tmpl"]).generate(post=post, show_subtitle=False, tags=tagsdir,
            show_featured=False, show_header=False, postid=postid, comments=post_comments_list, categories=categories)
        ofile = open(os.path.join(os.path.join(settings["site_path"] + "/posts/"), post["permalink"]),"wb")
        ofile.write(out)
        ofile.close()
    #
    # render the blog summary (with pagination)
    #
    render_blog_summary_with_pagination(post_list, featured, tagsdir, categories=categories)
    
    #
    # print post tagging information for all posts
    #
    print(50*"-")
    print("... {0:21s} : {1:3s} ".format("Posts tags info", "Nr:"))
    print(50*"-")
    for key in tagsdir:
        print( " ... {0:20s} : {1:3d} ".format(key,len(tagsdir[key])))
    #
    # finally render the tagviews
    # 
    render_tag_views(tagsdir, categories=categories)

def render_pages(exclude_files):
    '''
        render the pages
        pages differ from post in:
        * they have no summary list 
        * they have no tags
        * they cannot have comments (so far)

        ==> they represent a pure, sticky html page which can be anything you like
        It is sticky in that it will not move somwhere else when newer pages are created.
        <About> and <Projects> are typical examples for pages
        which you most probably want to add to your blog.

    '''
    print(50*"-")
    print("... Rendering the pages: ...")
    print(50*"-")
    page_list=[]
    categories=[]

    pages = [f for f in os.listdir(settings["page_path"]) if (isfile(join(settings["page_path"], f)) and f not in exclude_files)]
    #
    # get all pages / categories for the main menu navigation
    #
    for f in pages:
        filepath = os.path.join(settings["page_path"],f)
        mod_name,file_ext = os.path.splitext(os.path.split(filepath)[-1])
        #print("mod_name: " + str(mod_name) + " ::: " + str(file_ext) )
        py_mod = None
        permalink=""
        if file_ext.lower() == '.py':
            py_mod = imp.load_source(mod_name, filepath)
        if hasattr(py_mod, "page"):
            page = getattr(py_mod, "page")
            if not "permalink" in page:
                permalink = page["title"].lower().strip().replace(".", "").replace(" ", "-") + ".html"
            else:
                permalink += page["permalink"] + ".html"
            #
            # add the page to a category if any.
            #
            if page["nav_link"]:
                categories.append((page["nav_link_name"], "/pages/{}".format(permalink)))
            
    for f in pages:
        filepath = os.path.join(settings["page_path"],f)
        mod_name,file_ext = os.path.splitext(os.path.split(filepath)[-1])
        #print("mod_name: " + str(mod_name) + " ::: " + str(file_ext) )
        py_mod = None
        if file_ext.lower() == '.py':
            py_mod = imp.load_source(mod_name, filepath)
        if hasattr(py_mod, "page"):
            page = getattr(py_mod, "page")
            # check every post for publish date vs today.
            # if ok, add post to post_render_list
            if publish_post(page):
                #pp.pprint(post)
                if "title" in page:
                    print("... render page: " + page["title"], end="")
                page["delta_days"] = str(get_delta_days(page))
                page["markdown_text_list"] = []
                page["markdown_text"] = ""
                for t in page["text"]:
                    page["markdown_text_list"].append(markdown.markdown(str(t).strip()+ "<br>  ", extras=["fenced-code-blocks"]))
                    page["markdown_text"] += markdown.markdown(str(t).strip()+ "<br>  ", extras=["fenced-code-blocks"])
                #post["permalink"] = hashlib.md5(post["title"].encode(settings["encoding"])).hexdigest() + ".html"
                if not "permalink" in page:
                    page["permalink"] = page["title"].lower().strip().replace(".", "").replace(" ", "-") + ".html"
                else:
                    page["permalink"] += ".html"
                page["featured"] = False
                print(" : permalink -> " + page["permalink"])
                
                #
                # exec pre_renderer if there is one
                retval = None
                #
                # execute a pre renderer. (if any)
                # 
                if "pre_renderer" in page:
                    print(" ... executing pre_renderer: " + str(page["pre_renderer"]))
                    obj = page["pre_renderer"]()
                    func = getattr(obj, "run")
                    #print(" ... -> " + str(func))
                    retval = func(page=page)
                #
                # now render the page
                #
                #print("... now rendering: {}".format(page["template"]))
                out = loader.load(page["template"]).generate(page=page, show_subtitle=False, 
                    show_featured=False, show_header=False, current_page=page["nav_link_name"], custom=retval,
                    categories=categories)
                
                ofile = open(os.path.join(os.path.join(settings["site_path"] + "/pages/"), page["permalink"]),"wb")
                ofile.write(out)
                ofile.close()
                
                #print("...delta_days::: " + post["delta_days
                page_list.append(page)
        else:
            print(" ... has no attribute page ... " + mod_name)
    return categories

def cleanup():
    '''
        cleans the site dir to be prepared for a new run
    '''
    import shutil
    try:
        shutil.rmtree(settings["site_path"])
        print(" ... cleaned: " + settings["site_path"] + "directory ...")
    except FileNotFoundError as e:
        print(" ... Error: {}".format(str(e)))

    if not os.path.exists(settings["site_path"]):
        os.makedirs(settings["site_path"])
    if not os.path.exists(os.path.join(settings["site_path"], settings["post_path"])):
        os.makedirs(os.path.join(settings["site_path"], settings["post_path"]))
    if not os.path.exists(os.path.join(settings["site_path"], settings["page_path"])):
        os.makedirs(os.path.join(settings["site_path"], settings["page_path"]))
    if not os.path.exists(os.path.join(settings["site_path"], settings["tagview_path"])):
        os.makedirs(os.path.join(settings["site_path"], settings["tagview_path"]))
    if not os.path.exists(os.path.join(settings["site_path"], settings["data_path"])):
        os.makedirs(os.path.join(settings["site_path"], settings["data_path"]))
    #
    # copy the static folder
    #
    
    import distutils
    from distutils import dir_util
    distutils.dir_util.copy_tree(settings["static_path"], os.path.join(settings["site_path"], settings["static_path"]))
    print(" ... copied " + settings["static_path"])
    distutils.dir_util.copy_tree(settings["data_path"], os.path.join(settings["site_path"], settings["data_path"]))
    print(" ... copied " + settings["data_path"])


def save_comments(comments):
    #print(comments)
    print(" ... Saved " + str(len(comments)) + " comments")
    path = os.path.abspath(os.path.join(settings["data_path"], "comments.csv"))
    fieldnames= ["user","raw_text","postid","text","created_at", "profile_img_url", "hash"]
    #print(str(fieldnames))
    f= open(path, 'w')
    w = csv.DictWriter(f, fieldnames=fieldnames)
    w.writeheader()
    for elem in comments:
        #print(list(elem.items()))
        w.writerow(elem)
            

def load_comments():
    comments = []
    path = os.path.abspath(os.path.join(settings["data_path"], "comments.csv"))
    try:
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                comments.append(row)
    except FileNotFoundError as e:
        print(" ... comments load: " + str(e))
    return comments

def prepare_comments():
    #
    # get the comments
    # 
    comments = [] 
    comments_new = []

    if settings["enable_twitter_comments"]:
        #
        # get the new comments from twitter ?
        #
        choice = input(' ... -> Update Twitter comments now ? [y|N]: ').strip()
        if choice.lower() == "y":
            print(" ... Trying to collect new comments from Twitter")
            comments_new = twitter_comments.get_comments()
        else:
            print(" ... Not collecting comments from Twitter")
        
        comments = load_comments()
        #
        # merge the new and saved comments (avoid doulbes)
        # 
        #tmp = comments + comments_new
        # eliminate double dict entries from list: 
        # see: http://stackoverflow.com/questions/9427163/remove-duplicate-dict-in-list-in-python
        #comments = [dict(t) for t in set([tuple(sorted(d.items())) for d in tmp])]
        add = True
        out=[]
        for c1 in comments_new:
            for c2 in comments:
                if c1["hash"] == c2["hash"]:
                    add = False
                    print(" ... skipped comment: " + c1["text"])
            if add:
                print(" ... added comment: " +c1["text"])
                out.append(c1)
        out += comments
    #print(comments)
    save_comments(out)
    return comments

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='static.py -- The simple and quick static site generator ')

    parser.add_argument('-u', '--update-only', dest='update', action='store_true',
        default=False, help='only process posts that changed after the last run.')

    parser.add_argument('-d', '--domain-urls', dest='urls', action='store_true',
        default=False, help='create absolute links containing the real domain')




    args = parser.parse_args()
    #print("... Arguments:::" + str(args))

    print(50*"-")
    print("... generating blog ...")
    post_list = []

    # go to posts dir
    # get every post there (.py files with a post attribute)

    exclude_files = ["__init__.py", ".DS_Store"]
    
    comments = prepare_comments()

    #
    # cleanup site dir
    #
    cleanup()

    #
    # render the pages
    #    
    categories=render_pages(exclude_files)
    print(50*"-")
    print("... Pages summary:")
    print(50*"-")
    for elem in categories:
        print(" ... {0:21}:  {1:50}".format(str(elem[0]), str(elem[1])))
    #
    # render the posts
    #
    render_posts(exclude_files, comments, categories=categories)
        

 




